/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.google.ar.sceneform.samples.augmentedfaces;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.google.ar.sceneform.samples.augmentedfaces";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
}
