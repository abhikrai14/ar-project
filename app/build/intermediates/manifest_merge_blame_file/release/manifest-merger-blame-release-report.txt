1<?xml version="1.0" encoding="utf-8"?>
2<!--
3   Copyright 2019 Google LLC
4   Licensed under the Apache License, Version 2.0 (the "License");
5   you may not use this file except in compliance with the License.
6   You may obtain a copy of the License at
7
8      http://www.apache.org/licenses/LICENSE-2.0
9
10   Unless required by applicable law or agreed to in writing, software
11   distributed under the License is distributed on an "AS IS" BASIS,
12   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
13   See the License for the specific language governing permissions and
14   limitations under the License.
15-->
16<manifest xmlns:android="http://schemas.android.com/apk/res/android"
17    package="com.google.ar.sceneform.samples.augmentedfaces"
18    android:versionCode="1"
19    android:versionName="1.0" >
20
21    <uses-sdk
22        android:minSdkVersion="24"
22-->D:\Work\AR Android\sceneform-android-sdk-1.15.0\sceneform-android-sdk-1.15.0\samples\augmentedfaces\app\src\main\AndroidManifest.xml
23        android:targetSdkVersion="28" />
23-->D:\Work\AR Android\sceneform-android-sdk-1.15.0\sceneform-android-sdk-1.15.0\samples\augmentedfaces\app\src\main\AndroidManifest.xml
24    <!--
25         "AR Required" apps must declare minSdkVersion ≥ 24.
26         "AR Optional" apps must declare minSdkVersion ≥ 14
27    -->
28    <!-- Sceneform requires OpenGLES 3.0 or later. -->
29    <uses-feature
29-->D:\Work\AR Android\sceneform-android-sdk-1.15.0\sceneform-android-sdk-1.15.0\samples\augmentedfaces\app\src\main\AndroidManifest.xml:22:5-78
30        android:glEsVersion="0x00030000"
30-->D:\Work\AR Android\sceneform-android-sdk-1.15.0\sceneform-android-sdk-1.15.0\samples\augmentedfaces\app\src\main\AndroidManifest.xml:22:19-51
31        android:required="true" />
31-->D:\Work\AR Android\sceneform-android-sdk-1.15.0\sceneform-android-sdk-1.15.0\samples\augmentedfaces\app\src\main\AndroidManifest.xml:22:52-75
32    <!-- Always needed for AR. -->
33
34    <uses-permission android:name="android.permission.CAMERA" />
34-->D:\Work\AR Android\sceneform-android-sdk-1.15.0\sceneform-android-sdk-1.15.0\samples\augmentedfaces\app\src\main\AndroidManifest.xml:25:5-65
34-->D:\Work\AR Android\sceneform-android-sdk-1.15.0\sceneform-android-sdk-1.15.0\samples\augmentedfaces\app\src\main\AndroidManifest.xml:25:22-62
35    <!--
36         Indicates that this app requires Google Play Services for AR ("AR Required") and results in
37         the app only being visible in the Google Play Store on devices that support ARCore.
38         For an "AR Optional" app, remove this tag.
39    -->
40    <uses-feature
40-->D:\Work\AR Android\sceneform-android-sdk-1.15.0\sceneform-android-sdk-1.15.0\samples\augmentedfaces\app\src\main\AndroidManifest.xml:29:5-86
41        android:name="android.hardware.camera.ar"
41-->D:\Work\AR Android\sceneform-android-sdk-1.15.0\sceneform-android-sdk-1.15.0\samples\augmentedfaces\app\src\main\AndroidManifest.xml:29:19-60
42        android:required="true" />
42-->D:\Work\AR Android\sceneform-android-sdk-1.15.0\sceneform-android-sdk-1.15.0\samples\augmentedfaces\app\src\main\AndroidManifest.xml:29:61-84
43
44    <application
44-->D:\Work\AR Android\sceneform-android-sdk-1.15.0\sceneform-android-sdk-1.15.0\samples\augmentedfaces\app\src\main\AndroidManifest.xml:31:5-55:19
45        android:allowBackup="false"
45-->D:\Work\AR Android\sceneform-android-sdk-1.15.0\sceneform-android-sdk-1.15.0\samples\augmentedfaces\app\src\main\AndroidManifest.xml:32:9-36
46        android:appComponentFactory="android.support.v4.app.CoreComponentFactory"
46-->[com.android.support:support-compat:28.0.0] C:\Users\Abhi Rai\.gradle\caches\transforms-2\files-2.1\e740562fe37fff33c1bd3bd11147cdfe\support-compat-28.0.0\AndroidManifest.xml:22:18-91
47        android:extractNativeLibs="false"
48        android:icon="@drawable/ic_launcher"
48-->D:\Work\AR Android\sceneform-android-sdk-1.15.0\sceneform-android-sdk-1.15.0\samples\augmentedfaces\app\src\main\AndroidManifest.xml:33:9-45
49        android:label="@string/app_name"
49-->D:\Work\AR Android\sceneform-android-sdk-1.15.0\sceneform-android-sdk-1.15.0\samples\augmentedfaces\app\src\main\AndroidManifest.xml:34:9-41
50        android:theme="@style/AppTheme"
50-->D:\Work\AR Android\sceneform-android-sdk-1.15.0\sceneform-android-sdk-1.15.0\samples\augmentedfaces\app\src\main\AndroidManifest.xml:35:9-40
51        android:usesCleartextTraffic="false" >
51-->D:\Work\AR Android\sceneform-android-sdk-1.15.0\sceneform-android-sdk-1.15.0\samples\augmentedfaces\app\src\main\AndroidManifest.xml:36:9-45
52
53        <!--
54             Indicates that this app requires Google Play Services for AR ("AR Required") and causes
55             the Google Play Store to download and intall Google Play Services for AR along with
56             the app. For an "AR Optional" app, specify "optional" instead of "required".
57        -->
58        <meta-data
58-->D:\Work\AR Android\sceneform-android-sdk-1.15.0\sceneform-android-sdk-1.15.0\samples\augmentedfaces\app\src\main\AndroidManifest.xml:41:9-81
59            android:name="com.google.ar.core"
59-->D:\Work\AR Android\sceneform-android-sdk-1.15.0\sceneform-android-sdk-1.15.0\samples\augmentedfaces\app\src\main\AndroidManifest.xml:41:20-53
60            android:value="required" />
60-->D:\Work\AR Android\sceneform-android-sdk-1.15.0\sceneform-android-sdk-1.15.0\samples\augmentedfaces\app\src\main\AndroidManifest.xml:41:54-78
61
62        <activity
62-->D:\Work\AR Android\sceneform-android-sdk-1.15.0\sceneform-android-sdk-1.15.0\samples\augmentedfaces\app\src\main\AndroidManifest.xml:43:9-54:20
63            android:name="com.google.ar.sceneform.samples.augmentedfaces.AugmentedFacesActivity"
63-->D:\Work\AR Android\sceneform-android-sdk-1.15.0\sceneform-android-sdk-1.15.0\samples\augmentedfaces\app\src\main\AndroidManifest.xml:44:13-51
64            android:configChanges="orientation|screenSize"
64-->D:\Work\AR Android\sceneform-android-sdk-1.15.0\sceneform-android-sdk-1.15.0\samples\augmentedfaces\app\src\main\AndroidManifest.xml:46:13-59
65            android:exported="true"
65-->D:\Work\AR Android\sceneform-android-sdk-1.15.0\sceneform-android-sdk-1.15.0\samples\augmentedfaces\app\src\main\AndroidManifest.xml:49:13-36
66            android:label="@string/app_name"
66-->D:\Work\AR Android\sceneform-android-sdk-1.15.0\sceneform-android-sdk-1.15.0\samples\augmentedfaces\app\src\main\AndroidManifest.xml:45:13-45
67            android:screenOrientation="locked"
67-->D:\Work\AR Android\sceneform-android-sdk-1.15.0\sceneform-android-sdk-1.15.0\samples\augmentedfaces\app\src\main\AndroidManifest.xml:48:13-47
68            android:theme="@style/Theme.AppCompat.NoActionBar" >
68-->D:\Work\AR Android\sceneform-android-sdk-1.15.0\sceneform-android-sdk-1.15.0\samples\augmentedfaces\app\src\main\AndroidManifest.xml:47:13-63
69            <intent-filter>
69-->D:\Work\AR Android\sceneform-android-sdk-1.15.0\sceneform-android-sdk-1.15.0\samples\augmentedfaces\app\src\main\AndroidManifest.xml:50:13-53:29
70                <action android:name="android.intent.action.MAIN" />
70-->D:\Work\AR Android\sceneform-android-sdk-1.15.0\sceneform-android-sdk-1.15.0\samples\augmentedfaces\app\src\main\AndroidManifest.xml:51:17-69
70-->D:\Work\AR Android\sceneform-android-sdk-1.15.0\sceneform-android-sdk-1.15.0\samples\augmentedfaces\app\src\main\AndroidManifest.xml:51:25-66
71
72                <category android:name="android.intent.category.LAUNCHER" />
72-->D:\Work\AR Android\sceneform-android-sdk-1.15.0\sceneform-android-sdk-1.15.0\samples\augmentedfaces\app\src\main\AndroidManifest.xml:52:17-77
72-->D:\Work\AR Android\sceneform-android-sdk-1.15.0\sceneform-android-sdk-1.15.0\samples\augmentedfaces\app\src\main\AndroidManifest.xml:52:27-74
73            </intent-filter>
74        </activity>
75        <!-- The minimal version code of ARCore APK required for an app using this SDK. -->
76        <meta-data
76-->[com.google.ar:core:1.15.0] C:\Users\Abhi Rai\.gradle\caches\transforms-2\files-2.1\fc40444e8ff35da3207391b93df2b390\core-1.15.0\AndroidManifest.xml:33:9-35:41
77            android:name="com.google.ar.core.min_apk_version"
77-->[com.google.ar:core:1.15.0] C:\Users\Abhi Rai\.gradle\caches\transforms-2\files-2.1\fc40444e8ff35da3207391b93df2b390\core-1.15.0\AndroidManifest.xml:34:13-62
78            android:value="191106000" /> <!-- This activity is critical for installing ARCore when it is not already present. -->
78-->[com.google.ar:core:1.15.0] C:\Users\Abhi Rai\.gradle\caches\transforms-2\files-2.1\fc40444e8ff35da3207391b93df2b390\core-1.15.0\AndroidManifest.xml:35:13-38
79        <activity
79-->[com.google.ar:core:1.15.0] C:\Users\Abhi Rai\.gradle\caches\transforms-2\files-2.1\fc40444e8ff35da3207391b93df2b390\core-1.15.0\AndroidManifest.xml:37:9-43:80
80            android:name="com.google.ar.core.InstallActivity"
80-->[com.google.ar:core:1.15.0] C:\Users\Abhi Rai\.gradle\caches\transforms-2\files-2.1\fc40444e8ff35da3207391b93df2b390\core-1.15.0\AndroidManifest.xml:38:13-62
81            android:configChanges="keyboardHidden|orientation|screenSize"
81-->[com.google.ar:core:1.15.0] C:\Users\Abhi Rai\.gradle\caches\transforms-2\files-2.1\fc40444e8ff35da3207391b93df2b390\core-1.15.0\AndroidManifest.xml:39:13-74
82            android:excludeFromRecents="true"
82-->[com.google.ar:core:1.15.0] C:\Users\Abhi Rai\.gradle\caches\transforms-2\files-2.1\fc40444e8ff35da3207391b93df2b390\core-1.15.0\AndroidManifest.xml:40:13-46
83            android:exported="false"
83-->[com.google.ar:core:1.15.0] C:\Users\Abhi Rai\.gradle\caches\transforms-2\files-2.1\fc40444e8ff35da3207391b93df2b390\core-1.15.0\AndroidManifest.xml:41:13-37
84            android:launchMode="singleTop"
84-->[com.google.ar:core:1.15.0] C:\Users\Abhi Rai\.gradle\caches\transforms-2\files-2.1\fc40444e8ff35da3207391b93df2b390\core-1.15.0\AndroidManifest.xml:42:13-43
85            android:theme="@android:style/Theme.Material.Light.Dialog.Alert" />
85-->[com.google.ar:core:1.15.0] C:\Users\Abhi Rai\.gradle\caches\transforms-2\files-2.1\fc40444e8ff35da3207391b93df2b390\core-1.15.0\AndroidManifest.xml:43:13-77
86    </application>
87
88</manifest>
